{
  "openapi": "3.0.1",
  "info": {
    "title": "Universis signer",
    "description": "Universis signer client service specification",
    "contact": {
      "email": "support@universis.io"
    },
    "license": {
      "name": "GNU Lesser General Public License",
      "url": "https://gitlab.com/universis/universis-signer/-/blob/master/LICENSE"
    },
    "version": "1.8.0"
  },
  "externalDocs": {
    "description": "Find out more about Universis signer",
    "url": "https://gitlab.com/universis/universis-signer"
  },
  "servers": [
    {
      "url": "http://localhost:2465"
    }
  ],
  "paths": {
    "/keystore/certs": {
      "get": {
        "tags": [
          "signer"
        ],
        "summary": "Get certificates",
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Certificate"
                  }
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "content": {}
          },
          "403": {
            "description": "Forbidden",
            "content": {}
          },
          "500": {
            "description": "Internal Server Error",
            "content": {}
          }
        },
        "security": [
          {
            "basicAuth": [
              "usernamePassword"
            ]
          }
        ]
      }
    },
    "/sign": {
      "post": {
        "tags": [
          "signer"
        ],
        "summary": "Signs document",
        "description": "Signs a *.pdf, *.xlsx or *.docx document and returns the signed document.",
        "requestBody": {
          "content": {
            "multipart/form-data": {
              "schema": {
                "required": [
                  "file",
                  "image",
                  "thumbprint",
                  "otp"
                ],
                "properties": {
                  "file": {
                    "type": "string",
                    "format": "binary"
                  },
                  "image": {
                    "type": "string",
                    "format": "binary"
                  },
                  "thumbprint": {
                    "type": "string"
                  },
                  "otp": {
                    "type": "string"
                  },
                  "position": {
                    "type": "string"
                  },
                  "name": {
                    "type": "string"
                  },
                  "reason": {
                    "type": "string"
                  }
                }
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {}
          },
          "401": {
            "description": "Unauthorized",
            "content": {}
          },
          "403": {
            "description": "Forbidden",
            "content": {}
          },
          "500": {
            "description": "Internal Server Error",
            "content": {}
          }
        },
        "security": [
          {
            "basicAuth": [
              "usernamePassword"
            ]
          }
        ]
      }
    },
    "/verify": {
      "post": {
        "tags": [
          "signer"
        ],
        "summary": "Verifies the given document",
        "description": "Verifies the given document and returns signature properties, if any.",
        "requestBody": {
          "content": {
            "multipart/form-data": {
              "schema": {
                "required": [
                  "file"
                ],
                "properties": {
                  "file": {
                    "type": "string",
                    "format": "binary"
                  }
                }
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/VerifySignatureResult"
                  }
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "content": {}
          },
          "403": {
            "description": "Forbidden",
            "content": {}
          },
          "500": {
            "description": "Internal Server Error",
            "content": {}
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Certificate": {
        "type": "object",
        "properties": {
          "version": {
            "type": "integer",
            "example": 3
          },
          "subjectDN": {
            "type": "string",
            "example": "CN=Test Universis Signer, OU=Development Team, O=Universis Development Team, L=Thessaloniki, ST=Thessaloniki, C=GR"
          },
          "sigAlgName": {
            "type": "string",
            "example": "SHA256withRSA"
          },
          "sigAlgOID": {
            "type": "string",
            "example": "1.2.840.113549.1.1.11"
          },
          "issuerDN": {
            "type": "string",
            "example": "CN=Test Universis Signer, OU=Development Team, O=Universis Development Team, L=Thessaloniki, ST=Thessaloniki, C=GR"
          },
          "serialNumber": {
            "type": "integer",
            "format": "int64",
            "example": 1554329880452
          },
          "notAfter": {
            "type": "string",
            "example": "2021-07-15T14:59:44.000+00:00"
          },
          "notBefore": {
            "type": "string",
            "example": "2022-07-15T14:59:44.000+00:00"
          },
          "expired": {
            "type": "boolean",
            "example": false
          },
          "thumbprint": {
            "type": "string",
            "example": "3851b0b5e7e69067af5a9d43d0d9ce8533d8884d"
          },
          "commonName": {
            "type": "string",
            "example": "Test Universis Signer"
          }
        }
      },
      "VerifySignatureResult": {
        "type": "object",
        "properties": {
          "valid": {
            "type": "boolean",
            "example": true
          },
          "certificates": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Certificate"
            }
          },
          "signatureProperties": {
            "type": "object",
            "properties": {
              "signingDate": {
                "type": "string",
                "example": "2021-01-07T07:15:48.000+00:00"
              },
              "reason": {
                "type": "string",
                "example": "2021-01-07T07:15:48.000+00:00"
              },
              "signingCertificate": {
                "$ref": "#/components/schemas/Certificate"
              }
            }
          }
        }
      }
    },
    "securitySchemes": {
      "basicAuth": {
        "type": "http",
        "scheme": "basic"
      }
    }
  }
}