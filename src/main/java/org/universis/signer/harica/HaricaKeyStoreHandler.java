package org.universis.signer.harica;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.universis.signer.*;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Map;

public class HaricaKeyStoreHandler  implements RouterNanoHTTPD.UriResponder {
    private static final Logger log = LogManager.getLogger(KeyStoreHandler.class);
    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // get authorization header
        String[] usernamePassword;
        log.debug("get authorization header");
        String authorizationHeader = ihttpSession.getHeaders().get("authorization");
        String contentType = ihttpSession.getHeaders().get("content-type");
        log.debug("validate authorization header");
        if (authorizationHeader != null && authorizationHeader.startsWith("Basic ")) {
            // decode header
            log.debug("decode authorization header");
            byte[] decodedBytes = Base64.decodeBase64(authorizationHeader.replaceFirst("Basic ", ""));
            // and get username and password
            usernamePassword = new String(decodedBytes).split(":");
        } else {
            // otherwise throw forbidden error
            return new ForbiddenHandler().get(uriResource, map, ihttpSession);
        }
        try {
            // get certificates
            SignerAppConfiguration configuration = uriResource.initParameter(SignerAppConfiguration.class);
            if (configuration.keyStore == null) {
                return new ServerErrorHandler("Invalid application configuration. Service keystore cannot be empty at this context").get(uriResource, map, ihttpSession);
            }
            log.debug("format certificate message");
            HaricaSignerMessage message = new HaricaSignerMessage();
            message.username = usernamePassword[0];
            message.password = usernamePassword[1];
            log.debug("get certificates");
            HaricaCertificateMessageResult res = new HaricaSigner(configuration.keyStore).getCertificates(message);
            if (res.success) {
                log.debug("format response");
                NanoHTTPD.Response res1;
                ArrayList<X509CertificateInfo> certs = new ArrayList<>();
                if (res.data != null) {
                    if (res.data.certificates.size() > 0) {
                        // decode certificate
                        byte[] encodedCert = Base64.decodeBase64(res.data.certificates.get(0));
                        ByteArrayInputStream inputStream  =  new ByteArrayInputStream(encodedCert);
                        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
                        // and create an X509 cert
                        X509Certificate cert = (X509Certificate)certFactory.generateCertificate(inputStream);
                        // finally get X509 certificate info
                        X509CertificateInfo certInfo = new X509CertificateInfo(cert);
                        // and add it to collection
                        certs.add(certInfo);
                    }
                }
                res1 = new JsonResponseHandler(certs).get(uriResource, map, ihttpSession);
                CorsHandler.enable(res1);
                return res1;
            } else {
                return new ServerErrorHandler(res.error.message).get(uriResource, map, ihttpSession);
            }
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }
    }

    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
