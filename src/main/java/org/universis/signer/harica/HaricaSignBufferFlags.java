package org.universis.signer.harica;

public class HaricaSignBufferFlags {
    public static final int AR_SAPI_SIG_ENABLE_STS = 256;
    public static final int AR_SAPI_SIG_DISABLE_STS = 512;
    public static final int AR_SAPI_SIG_HASH_ONLY = 1024;
    public static final int AR_SAPI_SIG_PDF_REVOCATION = 4096;
    public static final int AR_SAPI_SIG_CAdES_REVOCATION = 8192;
    public static final int AR_SAPI_SHA256_FLAG = 16384;
    public static final int AR_SAPI_SHA384_FLAG = 32768;
    public static final int AR_SAPI_SHA512_FLAG = 65536;
    public static final int AR_SAPI_SIG_ENFORCE_NO_LOGOUT = 131072;
    public static final int AR_SAPI_SIG_ENFORCE_LOGOUT = 262144;
    public static final int AR_SAPI_SIG_PKCS1 = 524288;
    public static final int AR_SAPI_ENFORCE_SHA1_FLAG = 1048576;
    public static final int PERFORM_EXTENDED_VALIDATION = 1;

}
