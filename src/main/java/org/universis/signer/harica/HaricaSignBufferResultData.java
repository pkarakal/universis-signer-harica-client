package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HaricaSignBufferResultData {
    @JsonProperty("Signature")
    public String signature;
}
