package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HaricaSignBufferResult {
    @JsonProperty("Success")
    public Boolean success;

    @JsonProperty("Data")
    public HaricaSignBufferResultData data;

    @JsonProperty("ErrData")
    public HaricaSignBufferErrorResult error;
}
