package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HaricaCertificateMessageResultData {
    @JsonProperty("Subject")
    public String subjectDN;
    @JsonProperty("Issuer")
    public String issuerDN;
    @JsonProperty("SerialNumber")
    public String serialNumber;
    @JsonProperty("NotAfter")
    public Date notAfter;
    @JsonProperty("NotBefore")
    public Date notBefore;
    @JsonProperty("Thumbprint")
    public String thumbprint;
    @JsonProperty("Certificates")
    public ArrayList<String> certificates = new ArrayList<>();

    Certificate[] getCertificates() throws CertificateException {
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        ArrayList<Certificate> result = new ArrayList<>();
        for (String certificate : this.certificates) {
            InputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(certificate));
            result.add((Certificate) certFactory.generateCertificate(in));
        }
        Certificate[] a = new Certificate[result.size()];
        return result.toArray(a);
    }

}
