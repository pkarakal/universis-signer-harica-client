package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HaricaSignBufferMessage {
    @JsonProperty("Username")
    public String username;

    @JsonProperty("Password")
    public String password;

    @JsonProperty("SignPassword")
    public String otp;

    @JsonProperty("BufferToSign")
    public String buffer;

    @JsonProperty("Flags")
    public String flags;

}
