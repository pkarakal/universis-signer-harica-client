package org.universis.signer.harica;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.Objects;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class HaricaSignerTest {

    @org.junit.jupiter.api.BeforeAll
    public static void init() throws IOException {
        String trustStore = HaricaSignerTest.class.getResource("ssl-trust-keystore.p12").getPath();
        System.setProperty("javax.net.ssl.trustStore", trustStore);
        System.setProperty("javax.net.ssl.trustStoreType", "PKCS12");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

        Properties properties = new Properties();
        properties.load( new FileInputStream("tmp/test.properties"));
        System.setProperty("harica.username", properties.getProperty("harica.username"));
        System.setProperty("harica.password", properties.getProperty("harica.password"));
        System.setProperty("harica.otp", properties.getProperty("harica.otp"));

    }

    void shouldCreateInstance() throws URISyntaxException {
        final String serviceURI = "https://rsign-api-dev.harica.gr/";
        HaricaSigner signer = new HaricaSigner();
        assertNull(signer.serviceURI);
        signer.serviceURI = new URI(serviceURI);
        assertEquals(new URI(serviceURI), signer.getServiceURI());
        signer = new HaricaSigner(serviceURI);
        assertEquals(new URI(serviceURI), signer.getServiceURI());
        signer = new HaricaSigner(new URI(serviceURI));
        assertEquals(new URI(serviceURI), signer.getServiceURI());
        signer = new HaricaSigner();
        signer.setServiceURI(new URI(serviceURI));
        assertEquals(new URI(serviceURI), signer.getServiceURI());
    }

    void shouldSignDocumentGetError() throws IOException, URISyntaxException {
        // create message
        HaricaSignerMessage message = new HaricaSignerMessage();
        message.username = "johndoe";
        message.password = "secret";
        message.otp = "123456";
        message.attach(HaricaSignerTest.class.getResource("lorem-ipsum.pdf").getPath());
        // position is defined as optional but it must be set
        message.setSignaturePosition(new Rectangle(10, 10, 300, 120));
        HaricaSigner signer = new HaricaSigner("http://localhost:1010");
        assertThrows(HttpHostConnectException.class, () -> signer.sign(message));
        signer.serviceURI = new URI("https://rsign-api-dev.harica.gr/");
        HaricaSignerMessageResult res = signer.sign(message);
        assertFalse(res.success);
        assertNotNull(res.error);
        assertEquals( MessageResultErrorCodes.LogonFailure, res.error.innerCode);
    }

    void shouldSignDocument() throws IOException, URISyntaxException {
        // create message
        HaricaSignerMessage message = new HaricaSignerMessage();
        message.username = System.getProperty("harica.username");
        message.password = System.getProperty("harica.password");
        message.otp = System.getProperty("harica.otp");
        message.attach(HaricaSignerTest.class.getResource("lorem-ipsum.pdf").getPath());
        // position is defined as optional but it must be set
        message.setSignaturePosition(new Rectangle(10, 10, 300, 120));
        message.page = -1;
        HaricaSigner signer = new HaricaSigner("https://rsign-api.harica.gr/");
        HaricaSignerMessageResult res = signer.sign(message);
        assertTrue(res.success);
        String outFile = new java.io.File( "tmp/signed.pdf" ).getAbsolutePath();
        byte[] buffer = Base64.getDecoder().decode(res.data.signedFileData);
        OutputStream outStream = new FileOutputStream(outFile);
        outStream.write(buffer);
    }

    void shouldSignXlsxDocument() throws IOException, URISyntaxException {
        // create message
        HaricaSignerMessage message = new HaricaSignerMessage();
        message.username = System.getProperty("harica.username");
        message.password = System.getProperty("harica.password");
        message.otp = System.getProperty("harica.otp");
        message.signatureFieldName = "{470A2345-3B7B-446C-9F3B-61D61AB82583}";
        message.attach(Objects.requireNonNull(HaricaSignerTest.class.getResource("Book1.xlsx")).getPath());
        HaricaSigner signer = new HaricaSigner("https://rsign-api-dev.harica.gr/");
        message.setSignaturePosition(new Rectangle(10, 10, 300, 120));
        HaricaSignerMessageResult res = signer.sign(message);
        assertTrue(res.success);
        String outFile = new java.io.File( "tmp/signed.xlsx" ).getAbsolutePath();
        byte[] buffer = Base64.getDecoder().decode(res.data.signedFileData);
        OutputStream outStream = new FileOutputStream(outFile);
        outStream.write(buffer);
    }

    void shouldFindSignatureLine() throws IOException, URISyntaxException, InvalidFormatException {
        String fileName = HaricaSignerTest.class.getResource("Book1.xlsx").getPath();
        try {
            String signatureLineID = HaricaSigner.tryFindSignatureLine(fileName);
            assertNotNull(signatureLineID);
        } catch (ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
    }


    void shouldUseSignBuffer() throws IOException, URISyntaxException, InvalidFormatException, GeneralSecurityException {
        String message = "Hello World";
        MessageDigest messageDigest = MessageDigest.getInstance( "SHA-256");
        byte[] bytes = message.getBytes(StandardCharsets.UTF_8);
        byte[] messageHash = DigestAlgorithms.digest(new ByteArrayInputStream(bytes), messageDigest);
        assertNotNull(messageHash);

        HaricaSignBufferMessage bufferMessage = new HaricaSignBufferMessage();
        bufferMessage.username = System.getProperty("harica.username");
        bufferMessage.password = System.getProperty("harica.password");
        bufferMessage.otp = System.getProperty("harica.otp");
        bufferMessage.buffer = Base64.getEncoder().encodeToString(messageHash);

        HaricaSigner signer = new HaricaSigner("https://rsign-api-dev.harica.gr/");
        HaricaSignBufferResult result = signer.signBuffer(bufferMessage);
        assertTrue(result.success);
    }

    void shouldUseRemoteSignature() throws IOException, URISyntaxException, InvalidFormatException, GeneralSecurityException {
        byte[] bytes = "Hello World".getBytes(StandardCharsets.UTF_8);
        MessageDigest messageDigest = MessageDigest.getInstance( "SHA-256");
        byte[] hash = DigestAlgorithms.digest(new ByteArrayInputStream(bytes), messageDigest);
        HaricaSigner signer = new HaricaSigner("https://rsign-api-dev.harica.gr/");
        HaricaRemoteSignature remoteSignature = new HaricaRemoteSignature(
                signer,
                System.getProperty("harica.username"),
                System.getProperty("harica.password"),
                System.getProperty("harica.otp")
                );
        byte[] signature = remoteSignature.sign(hash);
        assertNotNull(signature);
    }

    void shouldSignWithRemoteSignature() throws IOException, GeneralSecurityException, DocumentException, InterruptedException {
        // create message
        HaricaSigner signer = new HaricaSigner("https://rsign-api-dev.harica.gr/");
        String outFile = new java.io.File( "tmp/signedPDF.pdf" ).getAbsolutePath();
        final HaricaSignerMessage message = new HaricaSignerMessage() {{
            username = System.getProperty("harica.username");
            password = System.getProperty("harica.password");
            otp = System.getProperty("harica.otp");
            page = 1;
        }};
        message.attach(Objects.requireNonNull(HaricaSignerTest.class.getResource("lorem-ipsum.pdf")).getPath());
        HaricaSignerMessageResult res = signer.sign(message, "http://qts.harica.gr", null);
        if (!res.success) {
            throw new DocumentException(res.error.message);
        }
        byte[] buffer = Base64.getDecoder().decode(res.data.signedFileData);
        OutputStream outStream = new FileOutputStream(outFile);
        outStream.write(buffer);
        outStream.close();
    }

    void shouldSignManyWithRemoteSignature() throws IOException, GeneralSecurityException, DocumentException, InterruptedException {
        // create message
        HaricaSigner signer = new HaricaSigner("https://rsign-api-dev.harica.gr/");

        class OneShotTask implements Runnable {
            final Integer index;
            final HaricaSigner signer;
            final HaricaSignerMessage message;
            OneShotTask(Integer _index, HaricaSigner _signer, HaricaSignerMessage _message) {
                index = _index;
                signer = _signer;
                message = _message;
            }
            public void run() {
                String outFile = new java.io.File( "tmp/signedPDF" + index + ".pdf" ).getAbsolutePath();
                try {
                    message.attach(HaricaSignerTest.class.getResource("lorem-ipsum.pdf").getPath());
                    HaricaSignerMessageResult res = signer.sign(message, "http://qts.harica.gr", null);
                    if (!res.success) {
                        throw new DocumentException(res.error.message);
                    }
                    byte[] buffer = Base64.getDecoder().decode(res.data.signedFileData);
                    OutputStream outStream = new FileOutputStream(outFile);
                    outStream.write(buffer);
                    outStream.close();
                } catch (IOException | DocumentException | GeneralSecurityException e) {
                    e.printStackTrace();
                }
            }
        }
        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(new OneShotTask(i, signer, new HaricaSignerMessage() {{
                username = System.getProperty("harica.username");
                password = System.getProperty("harica.password");
                otp = System.getProperty("harica.otp");
                page = 1;
            }}));
            t.start();
        }
    }

}